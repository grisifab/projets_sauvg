package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SpringBootMany21Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMany21Application.class, args);
	}

}
