package com.example.demo.dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.models.Adresse;
import com.example.demo.models.Course;

public interface AdresseRepository extends JpaRepository<Adresse, Long> {
	List<Adresse> findByPersonneId(Long personneId);
	Optional<Adresse> findByIdAndPersonneId(Long id, Long personneId);
}
