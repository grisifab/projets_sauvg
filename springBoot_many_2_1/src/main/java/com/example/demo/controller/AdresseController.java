package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.models.Adresse;
import com.example.demo.models.Course;
import com.example.demo.models.Adresse;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.dao.AdresseRepository;
import com.example.demo.dao.PersonneRepository;

@RestController
@RequestMapping("/api/v1")
public class AdresseController {
	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private PersonneRepository personneRepository;

	@GetMapping("/adresses")
	public List<Adresse> getAdresses() {
		return adresseRepository.findAll();
	}

	@GetMapping("/adresses/{id}")
	public ResponseEntity<Adresse> getAdresseById(@PathVariable(value = "id") Long adresseId)
			throws ResourceNotFoundException {
		Adresse user = adresseRepository.findById(adresseId)
				.orElseThrow(() -> new ResourceNotFoundException("Adresse not found :: " + adresseId));
		return ResponseEntity.ok().body(user);
	}
	
	@GetMapping("/personnes/{personneId}/adresses")
	public List<Adresse> getAdressesByPersonne(@PathVariable("personneId") Long personneId) {
		return adresseRepository.findByPersonneId(personneId);
	}
	
	
//	@PostMapping("/adresses")
//	public Adresse createUser(@Valid @RequestBody Adresse adresse) {
//		return adresseRepository.save(adresse);
//	}
	@PostMapping("/personnes/{personneId}/adresses")
	public Adresse save(@PathVariable(value = "personneId") Long personneId, @Valid @RequestBody Adresse adresse)
			throws ResourceNotFoundException {
		return personneRepository.findById(personneId).map(personne -> {
			adresse.setPersonne(personne);
			return adresseRepository.save(adresse);
		}).orElseThrow(() -> new ResourceNotFoundException("personne not found"));
	}


	@PutMapping("/adresses/{id}")
	public ResponseEntity<Adresse> updateUser(@PathVariable(value = "id") Long adresseId,
			@Valid @RequestBody Adresse userDetails) throws ResourceNotFoundException {
		Adresse user = adresseRepository.findById(adresseId)
				.orElseThrow(() -> new ResourceNotFoundException("Adresse not found :: " + adresseId));
		user.setRue(userDetails.getRue());
		user.setVille(userDetails.getVille());
		user.setCodePostal(userDetails.getCodePostal());
		final Adresse updatedUser = adresseRepository.save(user);
		return ResponseEntity.ok(updatedUser);
	}

	@DeleteMapping("/adresses/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long adresseId)
			throws ResourceNotFoundException {
		Adresse adresse = adresseRepository.findById(adresseId)
				.orElseThrow(() -> new ResourceNotFoundException("Adresse not found :: " + adresseId));
		adresseRepository.delete(adresse);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
