package com.example.demo;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.demo.dao.InstructorRepository;
import com.example.demo.models.Instructor;
import org.junit.jupiter.api.Test;

@RunWith(SpringRunner.class)
@DataJpaTest

class InstructorController {

	Instructor p1 = new Instructor("Morice", "Trintignant", "M.T.com");
	Instructor p2 = new Instructor("Boubou", "Foufou", "B.F.com");
	
	@Autowired
	private InstructorRepository instructorRepository;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	void testGetInstructors() {
		entityManager.persist(p1);
		entityManager.persist(p2);
		
		List<Instructor> lp1 = instructorRepository.findAll();
		List<Instructor> lp2 = new ArrayList<Instructor>();
		
		for (Instructor instructor : lp1) {
			lp2.add(instructor);
		}
		assertThat(lp2.size()).isEqualTo(2);	
	}

	@Test
	void testGetInstructorById() {
		Instructor pSavInDb = entityManager.persist(p1);
		Instructor pFromDb = instructorRepository.getOne(pSavInDb.getId());
		assertEquals(pSavInDb,pFromDb);
		assertThat(pFromDb.equals(pSavInDb));
	}

	@Test
	void testCreateUser() {
		Instructor pSavInDb = entityManager.persist(p1);
		Instructor pFromDb = instructorRepository.getOne(pSavInDb.getId());
		assertEquals(pSavInDb,pFromDb);
		assertThat(pFromDb.equals(pSavInDb));
	}

	@Test
	void testUpdateUser() {
		Instructor pSavInDb = entityManager.persist(p1);
		Instructor pFromDb = instructorRepository.getOne(pSavInDb.getId());
		pFromDb.setFirstName("Tiodio");
		entityManager.persist(pFromDb);
		pFromDb = instructorRepository.getOne(pSavInDb.getId());
		assertEquals(pFromDb.getFirstName(),"Tiodio");
	}

	@Test
	void testDeleteUser() {
		Instructor pSavInDb = entityManager.persist(p1);
		entityManager.persist(p2);
		entityManager.remove(pSavInDb);
		List<Instructor> lp1 = instructorRepository.findAll();
		List<Instructor> lp2 = new ArrayList<Instructor>();
		
		for (Instructor instructor : lp1) {
			lp2.add(instructor);
		}
		assertThat(lp2.size()).isEqualTo(1);
	}

}
