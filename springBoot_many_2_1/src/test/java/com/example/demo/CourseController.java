package com.example.demo;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import com.example.demo.models.Course;
import com.example.demo.models.Instructor;
import com.example.demo.dao.CourseRepository;
import com.example.demo.dao.InstructorRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class CourseController {
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private InstructorRepository instructorRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testSave() {
		Instructor instructor1 = new Instructor("admin", "admin", "a@a.fr");
		Course course = new Course("Java", instructor1);
		Course course1 = new Course("C++", instructor1);
		entityManager.persist(course);
		entityManager.persist(course1);
		Iterable<Course> AllCoursesFromDb = courseRepository.findAll();
		List<Course> courseList = new ArrayList<>();
		for (Course c : AllCoursesFromDb) {
			courseList.add(c);
			System.out.println("et de 1" + c);
		}
		assertThat(courseList.size()).isEqualTo(2);
	}

	@Test
	void testGetCoursesByInstructor() {
		Instructor instructor1 = new Instructor("admin", "admin", "a@a.fr");
		Course course = new Course("Java", instructor1);
		Course courseSavedInDb = entityManager.persist(course);
		List<Course> courseFromDb = courseRepository.findByInstructorId(instructor1.getId());
		assertThat(courseFromDb.contains(courseSavedInDb));
	}

	@Test
	void testUpdateCourse() {
		Instructor instructor1 = new Instructor("admin", "admin", "a@a.fr");
		Instructor instructor2 = new Instructor("admin2", "admin2", "a2@a.fr");
		Course course = new Course("Java", instructor1);
		entityManager.persist(course);
		Course getFromDb = courseRepository.getOne(course.getId());
		getFromDb.setTitle("Java/J2EE");
		entityManager.persist(getFromDb);
		assertThat(getFromDb.getTitle()).isEqualTo("Java/J2EE");
	}

	@Test
	void testDeleteCourse() {
		Instructor instructor1 = new Instructor("admin", "admin", "a@a.fr");
		Course course = new Course("Java", instructor1);
		Course course1 = new Course("C++", instructor1);
		Course persist = entityManager.persist(course);
		entityManager.persist(course1);
		entityManager.remove(persist);
		Iterable<Course> AllCoursesFromDb = courseRepository.findAll();
		List<Course> courseList = new ArrayList<>();
		for (Course c : AllCoursesFromDb) {
			courseList.add(c);
		}
		assertThat(courseList.size()).isEqualTo(1);
	}
}