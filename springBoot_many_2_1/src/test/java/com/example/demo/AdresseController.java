package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.AdresseRepository;
import com.example.demo.dao.InstructorRepository;
import com.example.demo.models.Adresse;
import com.example.demo.models.Instructor;
import com.example.demo.models.Personne;

@RunWith(SpringRunner.class)
@DataJpaTest

class AdresseController {
	
	Personne p1 = new Personne("Pipou", "Bibou", "pb@gmail.com");
	Adresse a1 = new Adresse("republique", "06452", "Auron", p1);
	
	@Autowired
	private AdresseRepository adresseRepository;
	
	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testPostandGetAdresses() {
		System.out.println("111111111111111111111111111111111111111111111111111111111111111111111111");
		entityManager.persist(p1);
		entityManager.persist(a1);
		List<Adresse> la1 = adresseRepository.findAll();
		Adresse a2 = la1.get(0);
		assertThat(a1).isEqualTo(a2);
		entityManager.remove(a1);
	}

	@Test
	void testGetAdresseById() {
		System.out.println("222222222222222222222222222222222222222222222222222222222222222222222222");
		entityManager.persist(p1);
		entityManager.persist(a1);
		Long ida1 = a1.getId();
		System.out.println("p1 :" + p1);
		System.out.println("a1 :" + a1);
		Optional<Adresse> a2 = adresseRepository.findById(ida1);
		System.out.println("a2 :" + a2);
		assertThat(a2.isPresent());
		if (a2.isPresent()) {
			assertThat(a1).isEqualTo(a2.get());
			entityManager.remove(a1);
		} else {
			fail("Non existant !");
		}
	}

	@Test
	void testGetAdressesByPersonne() {
		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333333333");
		entityManager.persist(p1);
		Long idp1 = p1.getId();
		entityManager.persist(a1);
		System.out.println("GABP a1 :" + a1);
		List<Adresse> la1 = adresseRepository.findByPersonneId(idp1);
		Adresse a2 = la1.get(0);
		System.out.println("GABP a2 :" + a2);
		assertThat(a1).isEqualTo(a2);
		entityManager.remove(a1);
	}
//	Instructor pSavInDb = entityManager.persist(p1);
//	Instructor pFromDb = instructorRepository.getOne(pSavInDb.getId());
//	pFromDb.setFirstName("Tiodio");
//	entityManager.persist(pFromDb);
//	pFromDb = instructorRepository.getOne(pSavInDb.getId());
//	assertEquals(pFromDb.getFirstName(),"Tiodio");

	@Test
	void testUpdateUser() {
		System.out.println("444444444444444444444444444444444444444444444444444444444444444444444444444");
		entityManager.persist(p1);
		entityManager.persist(a1);
		Long ida1 = a1.getId();
		Optional<Adresse> a2 = adresseRepository.findById(ida1);
		if (a2.isPresent()) {
			Adresse a3 = a2.get();
			a3.setRue("La nouvelle rue");
			a3.setVille("La nouvelle Ville");
			entityManager.persist(a3);
			a2 = adresseRepository.findById(ida1);
			if (a2.isPresent()) {
				assertThat(a2.get().getRue()).isEqualTo("La nouvelle rue");
				assertThat(a2.get().getVille()).isEqualTo("La nouvelle Ville");
			} else {
				fail("Non Present");
			}
		} else {
			fail("Non Present");
		}
		entityManager.remove(a1);
		System.out.println("Tuu a2 :" + a2);
	}

	@Test
	void testDeleteUser() {
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555");
		entityManager.persist(p1);
		entityManager.persist(a1);
		List<Adresse> la1 = adresseRepository.findAll();
		System.out.println("liste :" + la1);
		assertThat(la1.size()).isEqualTo(1);
		entityManager.remove(a1);
		la1 = adresseRepository.findAll();
		System.out.println("liste :" + la1);
		assertThat(la1.size()).isEqualTo(0);
	}

}
