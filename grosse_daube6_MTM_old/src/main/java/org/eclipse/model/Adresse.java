package org.eclipse.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity 
public class Adresse {

	@Id 
	private String rue; 
	private String codePostal; 
	private String ville;
	@ManyToMany(mappedBy="adresses")
	//@ManyToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	private List <Personne> personnes = new ArrayList <Personne> ();
	
	public void addPersonne(Personne personne) {
		this.personnes.add(personne);
		personne.getAdresses().add(this);
	}
	
	public List<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(List<Personne> personnes) {
		this.personnes = personnes;
	}

	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", codePostal=" + codePostal + ", ville=" + ville + "]";
	}
	public Adresse(String rue, String codePostal, String ville) {
		super();
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
	}
	public Adresse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}