package org.eclipse.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;


@Entity
 
public class Personne {
	
	@Id
	private int id;
	private String nom;
	private String prenom;
	
	@ManyToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE}) 
	private List <Adresse> adresses = new ArrayList <Adresse> ();

	public void addAdresse(Adresse adresse) {
		this.adresses.add(adresse);
		adresse.getPersonnes().add(this);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Adresse> getAdresses() {
		return adresses;
	}

	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}

	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", adresses=" + adresses + "]";
	}

	public Personne(int id, String nom, String prenom, List<Adresse> adresses) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.adresses = adresses;
	}
	
}
