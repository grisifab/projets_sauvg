package org.eclipse.main;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App 
{
    public static void main( String[] args )
    {
    	
    	Adresse adresse1 = new Adresse(); 
    	adresse1.setRue("berger"); 
    	adresse1.setCodePostal("14015"); 
    	adresse1.setVille("Menton");
    	
    	Adresse adresse2 = new Adresse(); 
    	adresse2.setRue("gerard"); 
    	adresse2.setCodePostal("13037"); 
    	adresse2.setVille("Valbonne");
    	
    	Adresse adresse3 = new Adresse(); 
    	adresse3.setRue("popo"); 
    	adresse3.setCodePostal("20035"); 
    	adresse3.setVille("Nissa");
    	
    	Adresse adresse4 = new Adresse(); 
    	adresse4.setRue("guimauve"); 
    	adresse4.setCodePostal("18037"); 
    	adresse4.setVille("Valbuena");
    	
    	Adresse adresse5 = new Adresse(); 
    	adresse5.setRue("pipo"); 
    	adresse5.setCodePostal("26035"); 
    	adresse5.setVille("Nisso");
    	
    	Personne personne = new Personne();  
    	personne.setId(1);
    	personne.setNom("nunono"); 
    	personne.setPrenom("Podra");
    	personne.addAdresse(adresse1);
    	personne.addAdresse(adresse2);
    	
    	Personne personne2 = new Personne();  
    	personne2.setId(2);
    	personne2.setNom("nanono"); 
    	personne2.setPrenom("Pidra");
    	personne2.addAdresse(adresse2);
    	personne2.addAdresse(adresse3);
    	
    	Personne personne3 = new Personne();  
    	personne3.setId(3);
    	personne3.setNom("nenono"); 
    	personne3.setPrenom("Padra");
//    	personne3.addAdresse(adresse4);
//    	personne3.addAdresse(adresse5);
    	
    	adresse4.addPersonne(personne3);
    	adresse5.addPersonne(personne3);
    	
    	Configuration configuration = new Configuration().configure(); 
    	SessionFactory sessionFactory = configuration.buildSessionFactory(); 
    	Session session = sessionFactory.openSession(); 
    	Transaction transaction = session.beginTransaction(); 
    	
    	session.persist(personne);
    	session.persist(personne2);
    	session.persist(personne3);
    	session.persist(adresse4);
    	session.persist(adresse5);
    	
    	transaction.commit(); 
    	session.close(); 
    	sessionFactory.close();

    }
}
