<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	<ul>
		<li><c:url value="http://localhost:8080/TP_session/FirstServlet" var="LFirst" /> 
			<a href="${ LFirst }">First</a></li>
		<li><c:url value="http://localhost:8080/TP_session/SecondServlet" var="LSecond" /> 
			<a href="${ LSecond }">Second</a></li>
		<li><c:url value="http://localhost:8080/TP_session/ThirdServlet" var="LThird" /> 
			<a href="${ LThird }">Third</a></li>
		<li><c:url value="http://localhost:8080/TP_session/DeconnectServlet" var="LDec" /> 
			<a href="${ LDec }">Deconnect</a></li>	
	</ul>
	
</body>
</html>