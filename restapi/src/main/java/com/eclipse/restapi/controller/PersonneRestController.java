package com.eclipse.restapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eclipse.restapi.dao.AdresseRepository;
import com.eclipse.restapi.dao.PersonneRepository;
import com.eclipse.restapi.models.Adresse;
import com.eclipse.restapi.models.Personne;

@Controller
public class PersonneRestController {
	@Autowired
	private PersonneRepository personneRepository;
	
	@Autowired
	private AdresseRepository adresseRepository;

	@GetMapping("/personnes")
	@ResponseBody
	public String getPersonnes() {
		return personneRepository.findAll().toString();
	}

	@GetMapping("/personnes/{id}")
	@ResponseBody
	public String getPersonne(@PathVariable("id") long id) {
		return personneRepository.findById(id).orElse(null).toString();
	}

//	@PostMapping("/personnes")
//	@ResponseBody
//	public String addPersonne(Personne personne) {
//		System.out.println(personne);
//		return personneRepository.save(personne).toString();
//	}
	
//	@PostMapping("/personnes")
//	@ResponseBody
//	public Personne addPersonne(@RequestBody Personne personne) {
//		System.out.println(personne);
//		return personneRepository.save(personne);
//	}
	
	@PostMapping("/personnes")
	public Personne addPersonne(@RequestBody Personne personne) {
		System.out.println(personne);
		List<Adresse> adresses = personne.getAdresses();
		for (Adresse adresse : adresses) {
			Adresse adr = null;
			if (adresse.getId() != null) {
				adr = adresseRepository.findById(adresse.getId()).orElse(null);
				adresses.set(adresses.indexOf(adresse), adr);
			} else {
				adr = adresseRepository.save(adresse);
			}
		}
		return personneRepository.saveAndFlush(personne);
	}
	
	@PutMapping("/personnes/{id}")
	public Personne updatePersonne(@PathVariable("id") long id, @RequestBody Personne personne) {
		personne.setNum(id);
		return personneRepository.save(personne);
	}	
	
	@DeleteMapping("/personnes/{id}")
	public boolean deletePersonne(@PathVariable("id") long id) {
		personneRepository.deleteById(id);
		return true;
	}
	
}