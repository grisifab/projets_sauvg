package com.eclipse.restapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import com.eclipse.restapi.models.Adresse;

public interface AdresseRepository extends JpaRepository<Adresse, Long> {
//	@Query("select a from Adresse a where a.rue = ?1")
//	List<Adresse> chercherSelonLaRue(String rue);	
//	List<Adresse> findByRueAndVille(String rue,String ville);
}
