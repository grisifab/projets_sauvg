package org.eclipse.spring_tp1;

import org.eclipse.model.Personne;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Personne p = context.getBean("per", Personne.class);
        System.out.println(p);
        p.afficher();
        
       
    }
}
