package org.eclipse.model;

import java.util.List;

public class Personne {
	private int id;
	private String nom;
	private String prenom;
	private Order order;
	private List<String> moyenPaye;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public List<String> getMoyenPaye() {
		return moyenPaye;
	}
	public void setMoyenPaye(List<String> moyenPaye) {
		this.moyenPaye = moyenPaye;
	}
	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", order=" + order + ", moyenPaye="
				+ moyenPaye + "]";
	}

	public void afficher() {
		System.out.println("Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", order=" + order + ", moyenPaye="
				+ moyenPaye + "]");
	}
	
}
