package org.eclipse.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity 
@DiscriminatorValue(value="ETU")
public class Etudiant extends Personne {
	
	private String niveau;

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return super.toString() + "Etudiant [niveau=" + niveau + "]";
	}

	public Etudiant(String nom, String prenom, String niveau) {
		super(nom, prenom);
		this.niveau = niveau;
	}

	public Etudiant() {
		super();
		// TODO Auto-generated constructor stub
	}

}
