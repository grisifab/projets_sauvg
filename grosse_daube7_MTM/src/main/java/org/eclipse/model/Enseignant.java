package org.eclipse.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity 
@DiscriminatorValue(value="ENS") 

public class Enseignant extends Personne {
	
	private int salaire;

	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	@Override
	public String toString() {
		return super.toString() + "Enseignant [salaire=" + salaire + "]";
	}

	public Enseignant(String nom, String prenom, int salaire) {
		super(nom, prenom);
		this.salaire = salaire;
	}

	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}
}
