package org.eclipse.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
//@Inheritance(strategy=InheritanceType.SINGLE_TABLE) // une seule table
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) // une table par classe /sous classe
@DiscriminatorColumn(name="TYPE_PERSONNE") 
@DiscriminatorValue(value="PERS")
 
public class Personne {
	
	private static int cnt;
	
	@Id
	private int id;
	private String nom;
	private String prenom;
	
	
	public int getId() {
		return id;
	}
	public void setId() {
		this.id = ++cnt;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	public Personne(String nom, String prenom) {
		super();
		this.id = ++cnt;
		this.nom = nom;
		this.prenom = prenom;
	}
	public Personne() {
		super();
		this.id = ++cnt;
		// TODO Auto-generated constructor stub
	}
	
	

	
	
}
