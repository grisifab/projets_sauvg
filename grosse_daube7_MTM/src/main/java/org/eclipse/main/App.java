package org.eclipse.main;


import org.eclipse.model.Enseignant;
import org.eclipse.model.Etudiant;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App 
{
    public static void main( String[] args )
    {
    	/* Personne */ 
    	Personne personne = new Personne(); 
    	personne.setNom("Guardiola"); 
    	personne.setPrenom("Pep"); 
    	
    	/* Enseignant */ 
    	Enseignant enseignant = new Enseignant(); 
    	enseignant.setNom("Ferguson"); 
    	enseignant.setPrenom("Sir"); 
    	enseignant.setSalaire(10000); 
    	
    	/* ´ etudiant */ 
    	Etudiant etudiant = new Etudiant(); 
    	etudiant.setNom("Mourinho"); 
    	etudiant.setPrenom("Jose"); 
    	etudiant.setNiveau("Ligue 1"); 
    	
    	
    	Configuration configuration = new Configuration().configure(); 
    	SessionFactory sessionFactory = configuration.buildSessionFactory(); 
    	Session session = sessionFactory.openSession(); 
    	Transaction transaction = session.beginTransaction(); 
    	
    	session.persist(personne);
    	session.persist(enseignant);
    	session.persist(etudiant);
    
    	
    	transaction.commit(); 
    	session.close(); 
    	sessionFactory.close();

    }
}
