package org.eclipse.models;

public class Personne {
	
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Personne [username=" + username + ", password=" + password + "]";
	}
	public Personne(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
