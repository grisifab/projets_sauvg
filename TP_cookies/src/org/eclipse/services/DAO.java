package org.eclipse.services;

import java.util.List;
import org.eclipse.models.Personne;


public interface DAO<T> {
	
	boolean validate(Personne o) throws ClassNotFoundException;

}
