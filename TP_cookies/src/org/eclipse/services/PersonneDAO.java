package org.eclipse.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.models.Personne;

public class PersonneDAO implements DAO<Personne> {
	public PersonneDAO() {
	}

	@Override
	public boolean validate(Personne o) throws ClassNotFoundException {
		boolean status = false;
		Class.forName("com.mysql.cj.jdbc.Driver");
		try (Connection connection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/webappj2ee?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC",
				"root", "root");
				PreparedStatement preparedStatement = connection
						.prepareStatement("select * from personne where username = ? and password = ? ")) {
			preparedStatement.setString(1, o.getUsername());
			preparedStatement.setString(2, o.getPassword());
			System.out.println(preparedStatement);
			ResultSet rs = preparedStatement.executeQuery();
			status = rs.next();
		} catch (SQLException e) {
			// process sql exception
			e.printStackTrace();
		}
		return status;
	}
}
