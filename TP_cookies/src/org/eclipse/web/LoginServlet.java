package org.eclipse.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.eclipse.models.Personne;
import org.eclipse.services.PersonneDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PersonneDAO personneDao;

	public void init() {
		personneDao = new PersonneDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		Integer id = Integer.parseInt(request.getParameter("id"));
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Personne personne = new Personne();
		personne.setUsername(username);
		personne.setPassword(password);
		PrintWriter out = response.getWriter();
		Cookie ck = new Cookie("Auth", username);
		ck.setMaxAge(60 * 60 * 24);
		try {
			if (personneDao.validate(personne)) {
				HttpSession session = request.getSession();
//				 session.setAttribute("id",id);
				response.addCookie(ck);
				session.setAttribute("username", username);
				response.sendRedirect("gestionCompte.jsp");
			} else {
				HttpSession session = request.getSession();
				// session.setAttribute("user", username);
				response.sendRedirect("index.jsp");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}