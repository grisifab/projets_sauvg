package org.eclipse.dallas;

public class velo {
	
	private int id;
	private int tailleRoue;
	private double poids;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTailleRoue() {
		return tailleRoue;
	}
	public void setTailleRoue(int tailleRoue) {
		this.tailleRoue = tailleRoue;
	}
	public double getPoids() {
		return poids;
	}
	public void setPoids(double poids) {
		this.poids = poids;
	}
	@Override
	public String toString() {
		return "velo [id=" + id + ", tailleRoue=" + tailleRoue + ", poids=" + poids + "]";
	}
	public velo(int id, int tailleRoue, double poids) {
		super();
		this.id = id;
		this.tailleRoue = tailleRoue;
		this.poids = poids;
	}
	public velo() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
