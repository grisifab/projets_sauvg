package org.eclipse.model;

public class Adresse {
	
	private String rue;
	private String codeP;
	private String ville;
	
	public Adresse(String rue, String codeP, String ville) {
		super();
		this.rue = rue;
		this.codeP = codeP;
		this.ville = ville;
	}
	
	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", codeP=" + codeP + ", ville=" + ville + "]";
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCodeP() {
		return codeP;
	}

	public void setCodeP(String codeP) {
		this.codeP = codeP;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}
	
	
	
}
