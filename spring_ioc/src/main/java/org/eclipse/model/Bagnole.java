package org.eclipse.model;

public class Bagnole {
	private String roue1;
	private String roue2;
	private String roue3;
	private String roue4;
	
	public Bagnole(String roue1, String roue2, String roue3, String roue4) {
		super();
		this.roue1 = roue1;
		this.roue2 = roue2;
		this.roue3 = roue3;
		this.roue4 = roue4;
	}

	public void afficher() {
		System.out.println("Bagnole [roue1=" + roue1 + ", roue2=" + roue2 + ", roue3=" + roue3 + ", roue4=" + roue4 + "]");
	}
	
	

}
