$(document).ready(function() {
	$('#userName').blur(function(event) { 	//quand on sort du champs de saisie de user name
		var name = $('#userName').val();  	// on récupère la valeur saisie
		$.get('GetUserServlet', {			// on appelle le servlet
			userName : name					// avec en paramètre la valeur saisie
		}, function(responseText) { 		// récupère sortie servlet dans response text
			$('#ajaxGetUserServletResponse').text(responseText); // balance vers jsp
		});
	});
});