package org.eclipse.main;

import org.eclipse.model.Personneaux;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Personneaux leGars = new Personneaux();
       leGars.setNom("Fassial");
       leGars.setPrenom("Jaquot");
       
       System.out.println("jusqu'ici tout va bien 1");
       Configuration configuration = new Configuration().configure();
       System.out.println("jusqu'ici tout va bien 2");
       SessionFactory sessionFactory = configuration.buildSessionFactory();
       System.out.println("jusqu'ici tout va bien 3");
       Session session = sessionFactory.openSession();
       System.out.println("jusqu'ici tout va bien 4");
       Transaction transaction = session.beginTransaction();
       System.out.println("jusqu'ici tout va bien 5");
       session.persist(leGars);
       System.out.println("jusqu'ici tout va bien 6");
       transaction.commit(); session.close(); sessionFactory.close();
       System.out.println("jusqu'ici tout va bien 7");

    }
}
